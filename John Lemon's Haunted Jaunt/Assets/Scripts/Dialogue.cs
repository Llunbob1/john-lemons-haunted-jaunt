﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dialogue : MonoBehaviour
{

	
	
	public bool GuiOn;


	
	
	public string Text = "INFOBOT: The gouls are trying to nuke the world, again. Using standard WASD controls, or your human equivalent, collect the blue and yellow uranium containers to stop them. Everything depends upon you not screwing up, or so my code tells me. Remember, you can only do your personal best, and if your personal best is insufficient it will be really embarassing for us both, but especially for you. Good luck!";

	
	public Rect BoxSize = new Rect(0, -200, 200, 100);


	
	
	public GUISkin customSkin;



	
	void OnTriggerEnter()
	{
		GuiOn = true;
	}


	void OnTriggerExit()
	{
		GuiOn = false;
	}

	void OnGUI()
	{

		if (customSkin != null)
		{
			GUI.skin = customSkin;
		}

		if (GuiOn == true)
		{
			
			GUI.BeginGroup(new Rect((Screen.width - BoxSize.width) / 2, (Screen.height - BoxSize.height) / 2, BoxSize.width, BoxSize.height));
			

			GUI.Label(BoxSize, Text);

			
			GUI.EndGroup();

		}


	}

}




